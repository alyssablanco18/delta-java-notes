public class student {
    // fields
    // access modifiers - public/private
    public String name;
    public String cohort;

    // private instance variable
    private double grade;

    // constructor
    // - should be the same name as the class
    public student(String studentName, String assignCohort) {
        name = studentName;
        cohort = assignCohort;
    }

    public student(String studentName) {
        name = studentName;
        cohort = "Unassigned to a cohort";
    }

    public student(String name, String cohort,double grade) {
        // this keyword provides us a way to refer to the CURRENT instance
        this.name = name;
        this.cohort = cohort;
        this.grade = grade;
    }

    public String getStudentInfo(){
        return String.format("Student Name: %s\nCohort Assigned: %s\n", name, cohort);
    }

    public String sayHello() {
        return "Hello i am " + this.name;
    }

    public double shareGrade() {
        return grade;
    }
} // END OF CLASS
