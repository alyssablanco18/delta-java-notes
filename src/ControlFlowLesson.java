import java.util.Locale;
import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {

        // COMPARISON OPERATORS
        // ==, >, <, <=, >=, !=
//        System.out.println(5 != 2); // true
//        System.out.println(5 >= 2); // true
//        System.out.println(5 <= 2); // true
//        System.out.println(5 > 5); // false

        // LOGICAL OPERATORS
        // &&, ||
//        System.out.println(5 == 6 && 2>1 && 3 != 3); // false
//        System.out.println(5 != 6 || 1<2 || 3 != 3); // true

        // .equals(), .equalsIgnoreCase()
        Scanner scanner = new Scanner(System.in);
//        System.out.println("would you like to start? [y/N]");
//        String userInput = scanner.nextLine();

        // DONT WANT TO DO!!
//        boolean confirm = userInput == 'y';

        // DO THIS INSTEAD!
//        boolean confirm = userInput.equals("y");
//
//        if (userInput.equals("y")) {
//            System.out.println("game has started good luck!");
//        }
//        else if (userInput.equalsIgnoreCase("n")) {
//            System.out.println("You chose to not start the game, goodbye");
//        }
//        else {
//            System.out.println("Answer not recognized...");
//        }



        // if statement
//        char letter = ';';
//        if (letter == 'y') {
//            System.out.println("the letter was y, i guess that means yes");
//        }
//        else if(letter == 'n') {
//            System.out.println(" so youre saying no?");
//        }
//        else {
//            System.out.println("i dont know what that means...");
//        }

        //==============================================================================
        //SWITCH STATEMENT
//        System.out.println("Enter a grade: ");
//        String userInput = scanner.nextLine().toUpperCase(Locale.ROOT);
//
//        switch (userInput) {
//            case "A":
//                System.out.println("Distinction Honors");
//                break;
//            case "B":
//                System.out.println("Grade: B");
//                break;
//            case "C":
//                System.out.println("Grade: C");
//                break;
//            case "D":
//                System.out.println("Grade: D");
//                break;
//            case "F":
//                System.out.println("Grade: F");
//                break;
//            default:
//                System.out.println("invalid entry");
//        }


        //======================================================================
//         WHILE LOOP
//        int i = 0;
//        while (i <= 10) {
//            System.out.println("i is " + i);
//            i++;
//        }

        //======================================================================
        // DO WHILE LOOP
//        int x = 0;
//        do {
//            System.out.println(x);
//            x++;
//        } while (x < 5);


        //=========================================================================
        // FOR LOOP
//        for (int y = 0; y < 10; y++); {
//            System.out.println(y);
//        }

        //==========================================================================
        // BREAK AND CONTINUE
        for (int a = 10; a <= 100; a++) {

            if (a == 21) {
                continue;
            }
            if (a == 89) {
                break;
            }

            System.out.println(a);

        }








    }
}
