package WishList;

import java.util.Scanner;

public class wishlistApp {

    public static void main(String[] args) {
        wishlistfeatures wishlist = new wishlistfeatures();
        do {
            System.out.println("What would you like to do ?\n");
            System.out.println(" 1 - add an item to the list\n");
            System.out.println(" 2 - display the list and total number of items\n");
            System.out.println(" 3 - exit");
            System.out.println();
            System.out.println("Enter a number 1 through 3 that corresponds...");

            Scanner sc = new Scanner(System.in);
            int userInput = sc.nextInt();

            if (userInput == 1) {
                wishlist.addingItem();
            } else if (userInput == 2) {
                System.out.println("List of items displayed here: ");
                wishlist.displayList();
            } else if (userInput == 3) {
                break;
            }



        } while (true);


    }

}
