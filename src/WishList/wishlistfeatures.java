package WishList;

import java.util.ArrayList;
import java.util.Scanner;

public class wishlistfeatures {


    ArrayList<wishlist> item = new ArrayList<>();

    public void addingItem() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter name of item: ");
        String itemInput = sc.nextLine();

        System.out.println("Enter price of item: ");
        int priceInput = sc.nextInt();

        wishlist addedItem = new wishlist(itemInput,priceInput);
        item.add(addedItem);

        System.out.println("Item has been added successfully to the list" );

        displayList();
    }

    // method
    public void displayList(){
        if (item.isEmpty()){
            System.out.println("0 items");
        }
        else {
            for (wishlist i : item)
                System.out.println(i.getName() + ": $" + i.getPrice());
            System.out.println(item.size());
        }
    }
////    public static void displayList() {
////        for (wishlist wish  : list) {
////            System.out.println(wish.getName() + ": $ " + wish.getPrice());
////        }
////    }
}
