import java.util.Arrays;

public class StringMethodsExercise {
    public static void main(String[] args) {
        // 1.
        String myLength = "Good afternoon, good evening, and good night";
        // Write some java code that will display the length of the string
//        System.out.println(myLength.length()); // returns 44

        // 2.
        // Use the string, but instead of using 'int myLength', use 'String uCase'
        String uCase = "Good afternoon, good evening, and good night";
        // Write some java code that will display the entire string in all
//        System.out.println(uCase.toUpperCase()); // returns the string in UPPERCASE

        // 3.
//        String uCase = "Good afternoon, good evening, and good night";
//        System.out.println(uCase.toLowerCase()); // returns the string in lowercase

        // 4.
        String firstSubstring = "Hello World".substring(6);
//        System.out.println(firstSubstring); // returns: World

        String firstSubstring3 = "Hello World".substring(3);
//        System.out.println(firstSubstring3); // returns: lo World

        String firstSubstring10 = "Hello World".substring(10);
//        System.out.println(firstSubstring10); // returns: d

        // 5.
        // Copy this code into your main methodString message =
        // "Good evening, how are you?Using the substring() method,
        // make your variable print out "Good evening"
        String message = "Good evening, how are you?";
//        System.out.println(message.substring(0,12)); // returns good evening

        String message1 = "Good evening, how are you?".substring(14);
//        System.out.println(message1); // returns how are you?

        // 6.
        String myChar = "San Antonio";

        // Using the charAt() method return the capital 'S' only.
//        System.out.println(myChar.charAt(0)); // returns the S

        // Change the argument in the charAt() method to where it
        // returns the capital 'A' only.
        String myChar1 = "San Antonio";
//        System.out.println(myChar1.charAt(4)); // returns: A

        // Change the argument in the charAt() method to where
        // it returns the FIRST lowercase 'o' only.Create a
        String myChar2 = "San Antonio";
//        System.out.println(myChar2.charAt(7)); // returns: first o

        // 6.
        //  create a String variable name 'alpha' and assign all the
        // names in the cohort in ONE string.
        String alpha = "Alyssa, Jose, Victor, Angela";
//        System.out.println(alpha);

        String splitAlpha = "Alyssa, Jose, Victor, Angela";
//        System.out.println(Arrays.toString(splitAlpha.split("  ")));

        // 7.
        // Using concatenation, print out all three string variables that
        // makes a complete sentence.
        String m1 = "Hello, ";
        String m2 = "how are you?";
        String m3 = " I love Java!";

//        System.out.println(m1 + m2 + "\n" + m3); // returns it in one sentence


        // Using concatenation, print out "You scored 89 marks for your
        // test!"
        // *89 should not be typed in the sout, it should be the integer
        // variable name.
        int result = 89;
//        System.out.println("you scored " + result + " marks for your test");


    } // end of main
} // end of class
