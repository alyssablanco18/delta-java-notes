package Shapes;
public class Square extends Quadrilateral{
    public Square(double side) {
        super(side, side);
    }
    @Override
    public double getArea() {
        return (4 * length);
    }
    @Override
    public double getPerimeter() {
        return (length * length);
    }
    @Override
    public void setLength(double length) {
    }
    @Override
    public void setWidth(double width) {
    }



} //  END OF CLASS
