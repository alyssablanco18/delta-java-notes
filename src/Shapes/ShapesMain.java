package Shapes;
public class ShapesMain {
    public static void main(String[] args) {

        Rectangle box1 = new Rectangle(5, 4);
        Rectangle box2 = new Rectangle(5, 3);

        Measurable myShape = new Rectangle(5,4);
        Measurable myShape1 = new Square(5);
        Measurable myShape3 = new Rectangle(7,5);

        System.out.println(myShape.getArea());
        System.out.println(myShape.getPerimeter());
        System.out.println(myShape3.getArea());
        System.out.println(myShape3.getPerimeter());
        System.out.println(myShape1.getArea());
        System.out.println(myShape1.getPerimeter());

    } // end of psvm

} // end of class
