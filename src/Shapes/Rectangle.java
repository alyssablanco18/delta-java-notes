package Shapes;
public class Rectangle extends Quadrilateral implements Measurable {
    protected double length;
    protected double width;
    //Constructor
    public Rectangle(double length, double width) {
        super(length, width);
        this.length = length;
        this.width = width;
    }
    @Override
    public double getArea() {
        return length * width;
    }
    @Override
    public double getPerimeter() {
        return (2 * (length + width));
    }
    @Override
    public void setLength(double length) {
    }
    @Override
    public void setWidth(double width) {
    }

//package Shapes;
//
//public class Rectangle {

//    // FIELDS
//    protected double length;
//    protected double width;
//
//    // CONSTRUCTOR
//    public Rectangle(double length, double width)
//    {
//        this.length = length;
//        this.width = width;
//    }
//
////    public Rectangle(Rectangle r) {
////        this.length = r.length;
////        this.width = r.width;
////    }
//
//    // METHODS
//    public void getArea() {
//        System.out.println((this.length * this.width));
////        System.out.println("This is the Area");
//    }
//
//    public void getPerimeter() {
//        System.out.println((2 * this.length + 2 * this.width));
////        System.out.println("This is the Perimeter");
//    }

}
