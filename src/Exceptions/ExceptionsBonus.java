package Exceptions;

import java.util.Scanner;

public class ExceptionsBonus {
    public static void main(String[] args) {
        ExceptionsBonus bonus = new ExceptionsBonus();
        System.out.println("Please Enter a Number: ");
        int userInput = bonus.getBinary();
        System.out.println(userInput);
    }

    Scanner sc = new Scanner(System.in);

    public int getBinary() {
        int output;

        try {
            output = Integer.valueOf(sc.nextLine(),2);
        }
        catch (Exception e) {
            System.out.println("Invalid Binary Number");
            return getBinary();
        }

        return output;
    }


    public int getHex() {
        int output;

        try {
            output = Integer.valueOf(sc.nextLine(),2);
        }
        catch (Exception e) {
            System.out.println("Invalid Binary Number");
            return getHex();
        }

        return output;
    }
}
