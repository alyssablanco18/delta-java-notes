package Exceptions;

import java.util.Scanner;

public class GeneralError {

    public static void main(String[] args) {

        // Create two integer variables, name them numerator,denominator.
        int numerator;
        int denominator;

        // Create a Scanner object name userInput and pass System.in as an argument.
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Enter the numerator: ");
            numerator = sc.nextInt();
            System.out.println("Enter the denominator: ");
            denominator = sc.nextInt();
            System.out.println("Result:  " + numerator / denominator);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("---End of Error Handling Example---");
        }
        // 12/4 returns: 3 and ---End of Error Handling Example---
        // 12/0 returns: / by zero and ---End of Error Handling

    } // end of psvm
}
