package Exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SpecificError {

    public static void main(String[] args) {

        int choice = 0;

        Scanner input = new Scanner(System.in);

        int[] numbers = {10, 11, 12, 13, 14, 15};
        System.out.println("Please enter the index of the array: ");

        try {
            choice = input.nextInt();
            System.out.printf("numbers[%d] = %d%n", choice, numbers[choice]);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error: Index is invalid");
        }
        catch (InputMismatchException e) {
            System.out.println("Error you did not enter a integer: ");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Enter 10 returns: Error: Index is invalid
        // Enter hello returns: Error you did not enter a integer:


        // BONUS attempted
//        int binary = 0;
//        public int binaryToInteger(String binary) {
//            char[] numbers = binary.toCharArray();
//            int result = 0;
//            for(int i=numbers.length - 1; i>=0; i--)
//                if(numbers[i]=='1')
//                    result += Math.pow(2, (numbers.length-i - 1));
//            return result;
//        }


    } // end of psvm

}
