import java.util.Random;

public class ServerNameGenerator {
        // ARRAY OF ADJECTIVES
        public static String[] ADJECTIVES = {
            "abandoned",
            "babyish",
            "calm",
            "damaged",
            "eager",
            "falling",
            "faint",
            "general",
            "generous",
            "handmade",
            "handy",
            "gentle"
        };

        // ARRAY OF NOUNS
        public static String[] NOUNS = {
            "people",
            "world",
            "health",
            "art",
            "music",
            "thanks",
            "reading",
            "theory",
            "control",
            "power",
            "ability",
            "idea"
        };


    // CREATE A METHOD THAT WILL RETURN A RANDOM ELEMENT FROM AN ARRAY OF STRINGS
    public static void Name(){
        Random random = new Random();
//        StringBuilder messageBuilder = new StringBuilder();
        System.out.println(ADJECTIVES[random.nextInt(ADJECTIVES.length)] + "-" + NOUNS [random.nextInt(NOUNS.length)]);
//        String message = messageBuilder.toString();
//        return message;
    }

    // STEPHENS SOLUTION
//    public static String getRandomWord(String[] words) {
//        Random random = new Random();
//        int randIndex = random.nextInt(words.length);
//        return words[randIndex];
//    }

    // MAIN METHOD
    public static void main(String[] args) {
//        System.out.println(Name());
        Name();
    }

    // STEPHENS SOLUTION
//    public static void main(String[] args) {
//
//    }

} // END OF CLASS
