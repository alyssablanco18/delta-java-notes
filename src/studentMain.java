public class studentMain {
    public static void main(String[] args) {
        // create two students
        student s1 = new student("karen");
        student s2 = new student("Miguel","Alpha");

        // create a third student
        student s3 = new student("KC","Bravo",91.5);

//        System.out.println(s1.name); // karen

        System.out.println(s2.sayHello()); // hello i am miguel

        System.out.println(s2.getStudentInfo());

        System.out.println(s3.shareGrade()); // 91.5
    }
}
