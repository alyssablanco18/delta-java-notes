public class CatMain {
    public static void main(String[] args) {
        // create two objects of the Cat class
        Cat cat1 = new Cat();
        Cat cat2 = new Cat();

        // define their states and behaviors
        cat1.name = "Momo";
        cat1.age = 3;
        cat1.breed = "Russian Blue";
        cat1.color = "Brown";


        // define our cat2
        cat2.name = "Thor";
        cat2.age = 7;
        cat2.breed = "Maine Coon";
        cat2.color = "Gray";


//        System.out.println(cat1.name); // Momo
//        cat1.sleep();
//
//        System.out.println(cat2.name); // Thor
//        cat2.eat();

        System.out.println(cat1.greeting());
        System.out.println(cat2.greeting());
        // NOTE: have to sout the greeting() method because its logic has a return statement

    }
}
