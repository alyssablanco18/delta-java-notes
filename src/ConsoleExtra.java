import java.util.Scanner;

public class ConsoleExtra {
    public static void main(String[] args) {
        // Console Extra

// 1. Write a Java program to print "Hello" in the console and then print your name on a separate line
        System.out.println("Hello\nAlyssa");

//2. Write a Java program that will print the sum of two numbers
        int x = 25;
        int y = 5;
        System.out.println(x + " + " + y + " = " + (x + y));
// or
        System.out.println(25 + 5);

//3. Write a Java program that takes two numbers as input and display the product of two numbers
//Test Data:
//Input first number: 25
//Input second number: 5
//Expected Output:
//25 x 5 = 125

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Please enter a whole number:");
//        int num1 = scanner.nextInt();
//        System.out.println("Please enter another whole number:");
//        int num2 = scanner.nextInt();
//        System.out.println(num1 + " x " + num2 + " = " + (num1 * num2));
////        or
//        System.out.printf("%d x %d = %d", num1, num2, num1*num2);


//4. Write a Java program to print the sum, multiply, subtract, divide, and remainder of two numbers
//Test Data:
//Input first number: 125
//Input second number: 24
//Expected Output :
//125 + 24 = 149
//125 - 24 = 101
//125 x 24 = 3000
//125 / 24 = 5
//125 % 24 = 5
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a whole number:");
        int num3 = scanner.nextInt();
        System.out.println("Please enter another whole number:");
        int num4 = scanner.nextInt();
        System.out.printf("%d + %d = %d \n", num3, num4, num3 + num4);
        System.out.printf("%d - %d = %d \n", num3, num4, num3 - num4);
        System.out.printf("%d x %d = %d \n", num3, num4, num3 * num4);
        System.out.printf("%d / %d = %d \n", num3, num4, num3 / num4);
        System.out.printf("%d  %d = %d \n", num3, num4, num3 % num4);




//5. Write a Java program that will take a string as an input and display the number of characters of that string
//Test Data:
//Input string:  pneumonoultramicroscopicsilicovolcanoconiosis
//Expected Output:
//Your string contains 45 characters.
//        System.out.println("Please enter a string:");
//        String userStr = scanner.nextLine();
//        int strLen = userStr.length();
//        System.out.println("Your string contains " + strLen + " characters.");
        System.out.println("Please enter a string:");





//6. Write a Java program that takes three numbers as input to calculate and print the average of the numbers.
        System.out.println("Please enter 3 numbers, hit enter after each entry");
        int userInput1 = scanner.nextInt();
        int userInput2 = scanner.nextInt();
        int userInput3 = scanner.nextInt();
        System.out.println("Thank you, the average of those 3 numbers is: " + ((userInput1 + userInput2 + userInput3)/3) );
    }
}
