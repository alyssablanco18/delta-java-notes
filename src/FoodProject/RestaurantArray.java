package FoodProject;

public class RestaurantArray {

    public static Restaurant[] showAllRestaurants() {
        return new Restaurant[] {
//                new Restaurant(),
                new Restaurant("Whataburger","burgers"),
                new Restaurant("Wendys","burgers"),
                new Restaurant("Pizza Hut","pizza"),
                new Restaurant("Dominoes","pizza"),
                new Restaurant("Chick-fil-a","chicken"),
                new Restaurant("Popeyes","chicken"),
                new Restaurant("Canes","chicken"),
                new Restaurant("Taco cabana","tacos"),
                new Restaurant("Chipotle","mexican")
        };
    }
}
