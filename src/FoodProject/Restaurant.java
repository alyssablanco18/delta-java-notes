package FoodProject;

public class Restaurant {
    // VARIABLES
    // what is a restaurant?
    private String name;
    private String type;

    // constructors
//    public Restaurant() {
//        System.out.println("Creating a new restaurant object...");
//    }

    public Restaurant(String n, String t) {
        this.name = n;
        this.type = t;
    }

    // method to show all restaurants
    public static String allRestaurants() {
        Restaurant[] restaurants = RestaurantArray.showAllRestaurants();
        String allRest = " ";
        for (Restaurant r : restaurants) {
            allRest += r.getName() + " - " + r.getType() + "\n";
        }
        return allRest;
    }

    // method for burger restaurants
    public static String burgerRestaurants() {
        Restaurant[] restaurants = RestaurantArray.showAllRestaurants();
        String burgerRestaurants = " ";
        for (Restaurant r : restaurants ) {
            if (r.getType().equals("burgers")) {
                burgerRestaurants += r.getName() + "\n";
            }
        }
        return burgerRestaurants;
    }

    // method for chicken restaurants


    // GETTERS & SETTERS
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
} // END OF CLASS
