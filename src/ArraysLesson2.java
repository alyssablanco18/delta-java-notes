import java.util.Arrays;

public class ArraysLesson2 {
    public static void main(String[] args) {
        String[] people = {"Ron", "Fred", "Sally"};
        System.out.println(Arrays.toString(people)); // returns [Ron, Fred, Sally]

        String[] morePeople = addPerson(people, "Kevin");
        System.out.println(Arrays.toString(morePeople)); // returns [Ron, Fred, Sally, Kevin]

    } // end of psvm

    public static String[] addPerson(String[] peopleArray, String person) {
        String[] peopleCopy = Arrays.copyOf(peopleArray, peopleArray.length + 1);

        peopleCopy[peopleCopy.length - 1] = person;
        // peopleCopy[lastIndexOf] = person

        return peopleCopy;


    }

} //  END OF CLASS
