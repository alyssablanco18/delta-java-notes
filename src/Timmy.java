import java.util.Scanner;

public class Timmy {
    public static void main(String[] args) {
    /*
    STRING BONUS:
    Create a class named Timmy with a main method for the following exercise.
    Timmy is a teenager with a short temper, his responses are very limited.
    Try to have a conversation with him.
    - If you ask him a question (an input that ends with a question mark), he will answer 'Sure'
    - If you yell at him (an input that ends with an exclamation mark), he will answers 'Yo, chill bruh'
    - If you address him without actually saying anything (empty input), he will say 'Fine, be that way!'
    - Anything else you say, he'll answer 'Yeah... sure'
    Write a java program so that a user can TRY to have a conversation with Timmy
    */
        Scanner sc = new Scanner(System.in);

        System.out.println("What will you say to timmy?");

        String userResponse = sc.nextLine();

        if (userResponse.endsWith("?")) {
            System.out.println("sure");
        }
        else if (userResponse.endsWith("!")) {
            System.out.println("Yo chill, Bruh");
        }
        else if (userResponse.equals("")) {
            System.out.println("Fine be that way!");
        }
        else{
            System.out.println("yeah..sure");
        }

    }
}
