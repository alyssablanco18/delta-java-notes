public class HelloWorld {
    // main method - shortcut psvm + enter
    public static void main(String[] args) {
        // sout
//        System.out.println("Hello World");
//        System.out.println("welcome to Java");

        // single line comments
        /*
        multi
        line
        comments
         */

        // STRINGS
        // - needs to be in " " (double quotations)
        // char - character
        // - needs yo be in ' ' (single quotations)
//        System.out.println('this will nto work');
//        System.out.println('C');

        // ESCAPE CHARACTERS
//        System.out.println("Escape Characters");
//        System.out.println("First \\"); //  \
//        System.out.println("Second \n");// new  line
//        System.out.println("Third \t tab character");// tab button


        // VARIABLES
        /*
        All variables in Java MUST be declared before they are used.
        Syntax:
        dataType nameOfVariable;
         */

        // EXAMPLES:
        byte age = 13;
        short myShort = -32768;

        int myInteger;
        myInteger = 18;

        boolean isAdmin;
        isAdmin = false;
//        System.out.println(isAdmin);

        /*
        CASTING:
        - Turning a value of one type into another
        - Two types of casting: implicit/ explicit casting
         */

        // IMPLICIT EXAMPLE
        int myInt = 900;
        long morePrecise = myInt;
        System.out.println(morePrecise);

        // EXPLICIT EXAMPLE
        double pi = 3.1459;
        int almostPi = (int) pi;
        System.out.println(almostPi);

        int a = 2;
        int b = 5;
//        System.out.println(a + b); // 7
    }
}
