import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {
        // Copy this code into your main method:double pi = 3.14159;
        double pi = 3.14159;


        // Write some java code that uses the variable pi to output the following:
        // The value of pi is approximately 3.14.
        System.out.printf("The value of pi is approximately %1.2f", pi);

        // Explore the Scanner ClassPrompt a user to enter a integer and store that
        Scanner user = new Scanner(System.in);
        System.out.println();
        System.out.println("Enter an Integer: ");

        // user has to enter integer or it will have errors
        int userInt = user.nextInt();
        System.out.println("The integer you entered was " + userInt);

        // Prompt a user to enter 3 words and store each of them in a
        // separate variable, then display them back, each on a newline.
//               String line;
        Scanner scanner = new Scanner(System.in);
        // first word
        System.out.println("Enter one word: ");
        String userInput = scanner.nextLine();
        System.out.println("Thank you, you entered: \n" + userInput);
        // second word
        System.out.println("Enter second word: ");
        String userInput2 = scanner.nextLine();
        System.out.println("Thank you, you entered: \n" + userInput2);
        // third word
        System.out.println("Enter third word: ");
        String userInput3 = scanner.nextLine();
        System.out.println("Thank you, you entered: \n" + userInput3);


        // Prompt a user to enter a sentence, then store that sentence in a
        // String variable using the .next method, then display that sentence
        // back to the user.
        //     * do you capture all of the words?-- the first word is missing
        System.out.println("Please enter a sentence: ");
        String userSentence = scanner.next();
        System.out.println("Thank you, you entered: \n" + userSentence);

        // Rewrite the above example using the .nextLine method. -- displays the
        // whole sentence.
        System.out.println("Please enter a sentence: ");
        String userSentence1 = scanner.nextLine();
        System.out.println("Thank you, you entered: \n" + userSentence1);


        // Calculate the perimeter and area of Code Bound's classrooms
        // Prompt the user to enter values of length and width of a
        // classroom at Code Bound.
        System.out.println("Please enter the length in feet: ");
        double cLength = scanner.nextDouble();
        System.out.println("Enter the width: ");
        double cWidth = scanner.nextDouble();
        System.out.println("The perimeter is " + (2*cLength + 2*cWidth));
        System.out.println("the area is " + (cWidth * cLength));



        // Use the .nextLine method to get user input and cast the resulting string
        // to a numeric type.    * Assume that the rooms are perfect rectangles.
        // * Assume that the user will enter valid numeric data for length and width.



        // Display the area and perimeter of that classroom.
        // The area of a rectangle is equal to the length times the width, and
        // the perimeter of a rectangle is equal to 2 times the length plus 2
        // times the width.


        // BONUSES...
        // * Accept decimal entries
        // * Calculate the volume of the rooms in addition to the area and perimeter
        System.out.println("Enter the volume: ");
        double h = scanner.nextDouble();
        System.out.println("The volume of the class is " + (h * cLength * cWidth));
    }
}
