import java.util.Scanner;

public class Television {
    // VARIABLES
    private String title;

    private int year;

    private String studio;

    // scanner bonus
    Scanner sc = new Scanner(System.in);
    public String getShow(){
        System.out.println("Please enter the name of a tv show: ");
        String input;
        input = sc.nextLine();

        System.out.println("Please enter the studio: ");
        String input2;
        input2 = sc.nextLine();

        System.out.println("Enter the year released: ");
        int input3;
        input3 = sc.nextInt();

        System.out.println( "You entered \n" + input + " produced by " + input2 + " and released in " + input3);
        return input + input2 + input3;
    } // end of scanner


    // CONSTRUCTOR
    // title
    public Television(){}

    public Television( String title, String studio, int year) {
        this.title = title;
        // SOLUTION ONE - PRINTS EACH VARIABLE IN NEW LINE
//        System.out.println("\n" + title);
//        System.out.println("\n" + studio);
//        System.out.println("\n" + year);

        // SOLUTION TWO - PRINTS THE VARIABLES IN A SENTENCE
        System.out.println("\n" + title + " produced by " + studio + " and released in " + year);
    }

    // STEPHENS SOLUTION
    // field/instance method
    /*
    Solution 2:
    public String displayInfo(){
    return String.format("%s came out in %d, and was produced by %s", title, year, studio);
     */
    /*
    GETTER:
    public String getTitle(){
    return title
    SETTER:
    public void setTitle(String title){
    this.title = title;
    GETTER:
    public String getStudio(){
    return studio;
    SETTER:
    public void setStudio(String studio){
    this.studio = studio;
    GETTER:
    public int getYear(){
    return studio;
    SETTER:
    public void setYear(int year){
    this.year = year;
     */


} // END OF CLASS
