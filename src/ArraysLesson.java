import java.lang.reflect.Array;
import java.util.Arrays;

public class ArraysLesson {
    public static void main(String[] args) {
        // syntax: type [] nameOfArray;
        int [] numbers;
        double[] prices;
        String[] names;

        String[] developers = new String[5];
        developers[0] = "Victor";
        developers[1] = "Angela";
        developers[2] = "Alyssa";
        developers[3] = "Jose";
        developers[4] = "Justin";

//        System.out.println(developers[3]); // returns Jose
//        System.out.println(developers.length); // returns 5

        /*
        what we did....
        Assigning a variable we created a new array where the size
        is determined by a constant.
         */

        int[] nums = new int[3];
        nums[0] = 11;
        nums[1] = 12;
//        nums[2] = "13"; // error: needs to match what we initialized for our array.
        nums[2] = 13;

        // ArrayIndexOutOfBoundsException
//        nums[3] = 14;
//        System.out.println(nums[3]);

        // javascript example
        // var numbers = [1, 2, 3, 4, 5];

        // Java
        String[] languages = {"html", "css", "javascript", "jquery", "angular", "java"};
        /*
        String[] languages = new String[6];
        languages[0] = "html";
        ...
         */
//        System.out.println(languages[3]); // returns jquery
//        System.out.println(languages.length); // returns 6


        // ITERATING ARRAYS

        // regular for loop
        // using our languages array...
        for (int i = 0; i < languages.length; i++) {
//            System.out.println(languages[i]);
//            System.out.println("Language: " + languages[i]);
//            System.out.println();
        }

        // enhanced for loop
        for (String language : languages) {
//            System.out.println("Enhanced For Loop: " + language);
        }


        // The Array Class
        String[] testArray = new String[4];
//
        Arrays.fill(testArray, "Badgers");
//        // = .fill fills all or a range of elements with a givin value
//        for (String element : testArray) {
//            System.out.println(element);
//        }


        // .toString() - prints out a copy of the array contents
//        System.out.println(Arrays.toString(testArray));

        // .equals() - returns true if two elements are equal and
        // in the same order.
        String[] words = {"hello", "goodbye"};
        String[] words2 = {"hello", "Goodbye"};
//        System.out.println(Arrays.equals(words,words2)); // returns false


        // .copyOf() - returns a copy array of a givin array of a givin length
        String[] myArray = new String[4];
        myArray[0] = "abby";
        myArray[1] = "brian";
        myArray[2] = "cathy";
        myArray[3] = "david";
        String[] myArray2 = Arrays.copyOf(myArray,2);
//        System.out.println(Arrays.toString(myArray));
        // returns [abby, brian, cathy, david]
//        System.out.println(Arrays.toString(myArray2));
        // retuns [abby, brian]



        // Arrays.sort(array, startIndex, toIndex)
        // sorts array elements alphabetically/numerically.
        // 2nd and 3rd are optional.

        String[] breakfast = {"eggs", "cereal", "milk", "pancakes"};

//        System.out.println(Arrays.toString(breakfast));
        String[] breakfastCopy = Arrays.copyOf(breakfast, breakfast.length);

        Arrays.sort(breakfastCopy);
//        System.out.println(breakfastCopy); // you have to wrap it in the toString Array
//        System.out.println(Arrays.toString(breakfastCopy));



        // two-dimensional arrays slide 6
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        System.out.println(Arrays.toString(matrix[0]));
        System.out.println(matrix[0][1]); // returns 2

        // return 9?
        System.out.println(matrix[2][2]); // returns 9

        // return 1?
        System.out.println(matrix[0][0]); // returns 1

        // return 6?
        System.out.println(matrix[1][2]); // returns 6

        for (int[] row : matrix) {
            System.out.println("+---+---+---+");
            System.out.print("| ");
            for (int c : row) {
                System.out.print(c + " | ");
            }
            System.out.println();
        }
        System.out.println("+---+---+---+");

    }
} // END OF CLASS
