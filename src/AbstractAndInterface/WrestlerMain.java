package AbstractAndInterface;

public class WrestlerMain {
    public static void main(String[] args) {

        Wrestler wrestler1 = new RicFlair();

        WWE wrestler2 = new SteveAustin();

        wrestler1.wrestlerName(); // returns Ric Flair
        wrestler1.finisherMove(); // returns Figure four leg lock
        wrestler1.paymentForPerformance(5); // returns The Wrestler pay for tonight: 1250

        wrestler2.wrestlerName(); // returns Stone Cold Steve Austin
        wrestler2.finisherMove(); // returns Stone cold stunner
        wrestler2.paymentForPerformance(5); // returns This WWE Wrestler pay: 2500
    }
}
