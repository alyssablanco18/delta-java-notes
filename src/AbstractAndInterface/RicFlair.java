package AbstractAndInterface;

public class RicFlair extends Wrestler {
    @Override
    public void wrestlerName() {
        System.out.println("Ric Flair");
    }

    @Override
    public void themeMusic() {
        System.out.println("Wooo!");
    }

    @Override
    public void finisherMove() {
        System.out.println("Figure four leg lock");
    }
}
