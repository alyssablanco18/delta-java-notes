package AbstractAndInterface;
/*
INTERFACE - much like abstract class in that they cannot be INSTANTIATED
Instead, they must be implemented by classes or extended by other interfaces
*** contain abstract methods only ***
 */
public interface WWE {

    // abstract methods
    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisherMove();

    public abstract void paymentForPerformance(int hours);
}
