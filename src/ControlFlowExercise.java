import java.util.Scanner;

public class ControlFlowExercise {
    public static void main(String[] args) {
//        // 1. WHILE LOOP
//        int i = 9;
//        while (i <= 23) {
//            System.out.print(i + " ");
//            i++;
//        } // end of while loop

 //=========================================================================

        // 2. DO WHILE
        // part one
//        int a = 0;
//        do {
//            System.out.println(a);
//            a = a + 2;
//        }while (a <= 100);

        // part two
//        int y = 100;
//        do{
//            System.out.println(y);
//            y = y - 5;
//        }while (y >= -10);

        // part three
//        long z = 2;
//        do {
//            System.out.println(z);
//            z *= z;
//        }while (z < 1000000);



 //=========================================================================

        // 3. display a table of powers
//        Scanner sc = new Scanner(System.in);
//        System.out.print("What number would you like to go up to? ");
//        int userInt = sc.nextInt();
//        System.out.println("");
//        System.out.println("Here is your table!");
//        System.out.println("");
//        System.out.println("number | squared | cubed");
//        System.out.println("------ | ------- | -----");
//
//        for (int i = 1; i <= userInt; i++){
//            System.out.println(i + "      | " + (i * i) + "       |" + "  " +(i * i * i));
//        }

 //===========================================================================

        // 4. Convert given numbers into letter grades.

        //construct a scanner object
        Scanner sc = new Scanner(System.in);

        System.out.println("Letter Grade Conversion");
        System.out.println(); //print a blank line

        //perform conversions until choice is value other than "y" or "Y"
        String choice = "y";
        while (choice.equalsIgnoreCase("y"))
        {
            //get input from the user
            System.out.print("Enter Numeric Grade(0-100):\t\t");
            double score = sc.nextDouble();

            //convert user numeric grade into letter grade
            char userGrade = 'F';
            if(score >= 88) {
                userGrade = 'A';
            }
            else if(score >= 80) {
                userGrade = 'B';
            }
            else if (score >= 67){
                userGrade = 'C';
            }
            else if (score >= 60) {
                userGrade = 'D';
            }
            // display conversion result
            String message = "Equivalent Letter Grade:\t" + userGrade;
            System.out.println(message);

            //see if user wants to continue
            System.out.print("\nContinue? (y/n):\t\t");
            choice = sc.next();
            System.out.println();
        }//end while loop
    }//end main()
}


