public class Variables {

    public static void main(String[] args) {

        int favoriteNum = 17;
        System.out.println(favoriteNum);

//        String myName = "alyssa" ;
//        System.out.println(myName);
//        char myName = "alyssa";
//        System.out.println(myName);
        // got error to change to String

        double myName = 3.14159;
        System.out.println(myName);

        // got an error: required type is string but requires double, once changed it worked
//        long myNum = (long) 3.14;
//        System.out.println(myNum);
        // got message to initialize the variable
        // once i assigned 3.14 to myNum i got another error saying "double" was provided
        // and asked if i wanted to cast to "long"
        // once casted it worked

//        float myNum = (float) 3.14;
        float myNum = 3.14f;
//        double myNum = 3.14;
        System.out.println(myNum);

//        long myNum = 123;
//        System.out.println(myNum);
        //just shows up as 123 with no L
        // 123 with no L just shows as 123 again
        // L is for a whole number




//        int x = 10;
//        System.out.println(x++); // returns 10 variable is used first then result calculated value currently is 11

//        System.out.println(x); // this one doesn't work because we are declaring the variable already but
        // returns 11 when fixed  value for x is 11 currently because we added one in previous code

        int y = 10;
        System.out.println(++y); // 11
        System.out.println(y); // 11

//        String class = "Hi"; // this is not allowed "class" is one of the naming conventions.
        String theNumberEight = "eight";
        System.out.println(theNumberEight); // returns eight

        Object o = theNumberEight;// error not recognized as a variable
//        int eight = (int) o;
        System.out.println(o);

//        int eight = (int) "eight";
//        System.out.println(eight);

        int x = 5;
//        x = x + 6;
        x+=6;
        System.out.println(x); // 11

//        int x = 7;
//        int y = 8;

        y = y * x;

        y*=x;
        System.out.println(y); // 392

        int a = 20;
        int b = 2;

        a/=b;
        System.out.println(a); // 10


        int z = Integer.MAX_VALUE;
        System.out.println(z); // 2147483647

        int zz = Integer.MIN_VALUE;
        System.out.println(zz); // -2147483648

        int zzz = Integer.MAX_VALUE + 1;
        System.out.println(zzz);

        long asdf = Long.MAX_VALUE;
        System.out.println(asdf); //9223372036854775807

        long fdsa = Long.MIN_VALUE;
        System.out.println(fdsa); //-9223372036854775808

    }
}
