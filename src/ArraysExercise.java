import java.util.Arrays;

public class ArraysExercise {
    public static void main(String[] args) {

        // number string
        int[] numbers = {3, 2, 5, 2, 4};

        System.out.println(Arrays.toString(numbers));

        int[] numbers2 = Arrays.copyOf(numbers, numbers.length);

        Arrays.sort(numbers2);
        System.out.println(Arrays.toString(numbers2));

        // adding a number to the array
        int[] moreNum = addNumber(numbers, 7);
        System.out.println(Arrays.toString(moreNum)); //  returns [3, 2, 5, 2, 4, 7]


        // people string
        String[] people = new String[3];
        people[0] = "Alyssa";
        people[1] = "Angela";
        people[2] = "Joseph";
        System.out.println(Arrays.toString(people));


        // adding a person to the array
        String[] morepeople = addToArray(people,"Savannah");
        System.out.println(Arrays.toString(morepeople));

        // digits
        int[] digits = {1, 2, 3};
        System.out.println("Sum of array is: " + sumOfArray(digits));

        // greeting
        String[] greetings = {"hello", "goodbye", "goodnight"};
        System.out.println(stringTogether(greetings));

    } // end of psvm


    // adding a number
    public static int[]  addNumber(int[] numbersArray, int number) {
        int[] numberCopy = Arrays.copyOf(numbersArray, numbersArray.length + 1);
        numberCopy[numberCopy.length - 1] = number;
        return numberCopy;
    }

    // adding a person
    public static String[] addToArray(String[] peopleArray, String person) {
        String[] peopleCopy = Arrays.copyOf(peopleArray, peopleArray.length + 1);
        peopleCopy[peopleCopy.length - 1] = person;
        return peopleCopy;
    }

    // sumOfArray method
    public static int sumOfArray(int[] digits){
        int sum = 0;
        int i;
        for (i = 0; i < digits.length; i++)
            sum += digits[i];
        return sum;
    }

    // stringTogether method
    public static String stringTogether(String[] stArray) {
        String allStr = "";
        for (String eachStr : stArray) {
            allStr += eachStr;
        }
        return allStr;
    }

//    public static String[] stringTogether(String[] greetingArray, String g) {
//        String[] greetingCopy = Arrays.copyOf(greetingArray, greetingArray.length + 1);
//        greetingCopy[greetingArray.length - 1] = g;
//        return greetingCopy;
//    }

} // END OF CLASS
