public class Car {
    public String maker;
    public String model;
    private double year;

    // constructor
    public Car(String carMaker, String carModel) {
        maker = carMaker;
        model = carModel;
    }

    public Car(String carMaker) {
        maker = carMaker;
        model = "Unassigned to a car";
    }

    public Car(String maker, String model, double year) {
        this.maker = maker;
        this.model = model;
        this.year = year;
    }

    public String getCarInfo() {
        return String.format("Car Make: %s\n Model: %s\n", maker, model);
    }

    public double getYear() {
        return year;
    }
}
