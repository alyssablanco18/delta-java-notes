import java.util.ArrayList;
import java.util.Collections;

public class CollectionsArraysExercise {
    public static void main(String[] args) {

        // 1. Create an ArrayList of integers
        ArrayList<Integer> myNums = new ArrayList<>();
        myNums.add(15);
        myNums.add(27);
        myNums.add(45);
        myNums.add(81);
//        System.out.println(myNums); // [15, 27, 45, 81]

        // 2. Create an ArrayList of Strings
        ArrayList<String> icecreams = new ArrayList<>();
        icecreams.add("chocolate");
        icecreams.add("vanilla");
        icecreams.add("strawberry");
        icecreams.add("coffee");
//        System.out.println(icecreams); // [chocolate, vanilla, strawberry, coffee]

        for (String name : icecreams) {
//            System.out.println(icecreams);
        }

        for (int i = 0; i < icecreams.size(); i++) {
//            System.out.println(icecreams.get(i));
        }

        icecreams.add(0,"cookies");
//        System.out.println(icecreams);
        // returns: [cookies, chocolate, vanilla, strawberry, coffee]


        // 3.
        ArrayList<String> myClass = new ArrayList<>();
        myClass.add("Jose");
        myClass.add("Alyssa");
        myClass.add("Victor");
        myClass.add("Angela");
//        System.out.println("This is my class: " + myClass);
        // returns: This is my class: [Jose, Alyssa, Victor, Angela]

        // 4.
        // Create a program to insert elements into the array list.
        // This element should be added on the first element and last.
        // You can use a previous array or create a new one.
        icecreams.add("cookies n cream");
//        System.out.println("My new icecream array: " + icecreams);
        // returns: My new icecream array: [chocolate, vanilla, strawberry, coffee, cookies n cream]

        icecreams.add("rocky road");
//        System.out.println(icecreams);
        // returns: [chocolate, vanilla, strawberry, coffee, cookies n cream, rocky road]


        // 5.
        // Create a program to remove the third element from an array list.
        // You can create a new array or use a previous one.
        icecreams.remove(2);
//        System.out.println(icecreams);
        // returns: [chocolate, vanilla, strawberry, cookies n cream, rocky road]

        // 6. Create an array of Dog breeds
        ArrayList<String> dogBreeds = new ArrayList<>();
        dogBreeds.add("Chihuahua");
        dogBreeds.add("pitbull");
        dogBreeds.add("doberman");
        dogBreeds.add("husky");
        dogBreeds.add("akita");
        System.out.println(dogBreeds);
        // returns: [Chihuahua, pitbull, doberman, husky, akita]

        // Create a program that will sort the arrayList
        java.util.Collections.sort(dogBreeds);
        System.out.println(dogBreeds);
        // returns:  [Chihuahua, akita, doberman, husky, pitbull]

        // 7. Create an array of cat breeds
        ArrayList<String> catBreeds = new ArrayList<>();
        catBreeds.add("Maine coon");
        catBreeds.add("Exotic");
        catBreeds.add("Siamese");
        catBreeds.add("Ragdoll");
        catBreeds.add("Persian");
        System.out.println(catBreeds);
        // returns: [Maine coon, Exotic, Siamese, Ragdoll, Persian]

//        if (catBreeds.contains("Persian")) {
//            System.out.println("is on the list");
//        }
//        else {
//            System.out.println("is not on the list");
//        }

        // ternary operator
        System.out.println(catBreeds.contains("Persian") ? "is on the list" : "is not on the list");

        // reverse the array
        java.util.Collections.reverse(catBreeds);
        System.out.println(catBreeds);
        // returns: [Persian, Ragdoll, Siamese, Exotic, Maine coon]











    } // end of psvm
} // end of class
