package ObjectsReview;

public class AthleteMain {
    public static void main(String[] args) {
        // instantiate the object of our class
        // ClassName objectName = new ClassName();
        Athlete tim = new Athlete();
        tim.firstName = "Tim";
        tim.lastName = "Duncan";

        // expected output: Hello from Tim Duncan
        System.out.println(tim.sayHello());

        tim.jerseyNumber = 21;
        System.out.println(tim.jerseyNumber); // returns 21
        tim.team = "San Antonio Spurs";

        Athlete michael = new Athlete();
        michael.firstName = "Michael";
        michael.lastName = "Jordan";

        // expected output: Hello from Michael Jordan
        System.out.println(michael.sayHello());
    }
}
