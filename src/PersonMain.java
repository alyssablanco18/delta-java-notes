public class PersonMain {
    public static void main(String[] args) {

            Person p1 = new Person("Sally");

            Person person1 = new Person("John");
//            Person person2 = new Person("John");
            Person person2 = new Person("Jane");

//        System.out.println(person1.getName().equals(person2.getName())); // true
//        System.out.println(person1 == person2);
//            Person person2 = person1;
            System.out.println(person1.getName()); // John

//            System.out.println(person2.getName()); // John

            System.out.println(person2.getName()); // Jane

            person1.sayHello(); // Hello!
        }

    }

