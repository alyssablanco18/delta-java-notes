package Methodsreview;

import java.util.Locale;

public class MethodLecture {
    public static void main(String[] args) {
        System.out.println(printNumber());
        sayHello();

        System.out.println(printFive());

        // call yell() and print it in the console?
        System.out.println(yell("whatever we're going to say"));

        // call yell() method that will print out a number in the console?
        System.out.println("MethodLecture class: " + yell(5));
    }

    /*
    method definition
    public static returnType nameOfMethod(param1, param2, ... ]) {
           // the statements of the method
           // a return statement, if a return type is declared in the method definition
       }

     */

    public static int printNumber() {
        return 1;
    }

    public static void sayHello() {
        System.out.println("Hello");
    }

    public static long printFive() {
        return 5;
    }

    // what is the name of the method? printFive
    // what is the returnType? long

    public static String yell(String str) {
        return str.toUpperCase() + "!!!";
    }

    // what is the name of the method? yell
    // what is the return type? String
    // what is the name of the parameter? str

    // method overloading...
    // having the same method name but with different sets of parameters
    public static String yell() {
        return "i dont know what we're yelling about!";
    }
//
    public static  int yell(int num) {
        return num;
    }
}
