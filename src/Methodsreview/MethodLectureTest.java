package Methodsreview;

public class MethodLectureTest {
    // create an object of our methodLecture class
    // "instantiating an object"
    /*
    className objectName = new className(arguments);
     */

    public static void main(String[] args) {
        // create an object of our method lecture class
        // "instantiating an object"
        /*
        ClassName objectName = new ClassName(arguments);
         */
        MethodLecture m1 = new MethodLecture();
        System.out.println(m1.printNumber()); // returns 1
        m1.sayHello(); // return hello

        System.out.println(m1.printFive()); // returns 5


        // access yell() from the Methodlecture class and print a string value?
        System.out.println(m1.yell("message here.."));

        // access the yell() method from the Methodlecture class
        // that print the return statement of the method that does not have any parameter
        System.out.println("MethodLecture test: " + m1.yell());
    }
}
