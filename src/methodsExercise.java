import java.util.Scanner;

public class methodsExercise {
    public static void main(String[] args) {
        // 1.
//        System.out.println(addNumbers(3, 5));
//        System.out.println(subNumbers(8, 2));
//        System.out.println(multiplyNumbers(6, 6));
//        System.out.println(divideNumbers(10, 5));


        // 4.
        // Input the first number: 25
        // Input the second number: 45
        // Input the third number: 65
        // Expected Output:The average value is 45.0
        Scanner in = new Scanner(System.in);
        System.out.print("Input the first number: ");
        double x = in.nextDouble();
        System.out.print("Input the second number: ");
        double y = in.nextDouble();
        System.out.print("Input the third number: ");
        double z = in.nextDouble();
        System.out.print("The average value is " + average(x, y, z)+"\n" );
    }
    // 1.
    // Create four separate methods.
    // Each will perform an arithmetic operation:
    // Addition
    public static int addNumbers(int num1, int num2){
        return num1 + num2;
    }
    // Subtraction
    public static int subNumbers(int num1, int num2){
        return num1 - num2;
    }
    // Multiplication
    public static int multiplyNumbers(int num1, int num2){
        return num1 * num2;
    }
    // Division
    public static int divideNumbers(int num1, int num2){
        return num1 / num2;
    }

    // Create a method that validates that the user input is in a certain range
//    public static void getInteger(int min, int max) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("enter a valid number:");
//        int userInput = scanner.nextInt();
//
//        if (userInput >= min && userInput <= max) {
//            System.out.println("valid number!");
//            System.out.println("you entered: " + userInput);
//            return userInput;
//        }
//        else {
//            System.out.println("invalid entry: number must be between " + min + "-" + max);
//            return getInteger(min, max);
//        }
//    }


    //Calculate the factorial of a number
    //-Prompt the user to enter an integer from 1 to 10-Display the factorial of the number
    // entered by the user-Ask if the user wants to continue-Use a for loop to calculate the
    // factorial-Assume that the user will enter an integer, but verify it's between 1 and 10
    // -Use the long type to store the factorial-Continue only if the user agrees to-A factorial
    // is a number multiplied by each of the numbers before it-Factorials are denoted by the
    // exclamation point (n!) oEx.o1! = 1= 1o2! = 1 x 2= 2o3! = 1 x 2 x 3= 6o4! = 1 x 2 x 3 x 4 = 24
    // Write a Java method to compute the average of three numbersTest data:Input the first number:
    // 25Input the second number: 45Input the third number: 65Expected Output:The average value is 45.0
//    public static void calculateFactorial() {
//
//        Scanner scanner = new Scanner(System.in);
//
//        int userInput;
//
//        do {
//            System.out.println("\n Please enter a number between 1 and 10: ");
//
//            userInput = scanner.nextInt();
//
//        }while (userInput < 1 || userInput > 10);
//
//        long result = 1L;
//    }



    // 4.
    // Write a Java method to compute the average of three numbers.
    public static double average(double x, double y, double z)
    {
        return (x + y + z) / 3;
    }

}
