import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CollectionsHashMapExercise {
    public static void main(String[] args) {

        HashMap<String , String> usernames = new HashMap<>();

        usernames.put("Stephen","stephen001");
        usernames.put("Angela", "Angie101");
        usernames.put("Alyssa", "Alyssa505");
        usernames.put("Jose", "JoseV211");
        usernames.put("Victor", "Victor572");
        System.out.println(usernames);
        // returns: {Victor=Victor572, Jose=JoseV211, Alyssa=Alyssa505, Angela=Angie101, Stephen=stephen001}

        usernames.put("Sally","sally323");
        System.out.println(usernames);
        // returns:

        // test if hashmap is empty
        System.out.println(usernames.isEmpty() ? "HashMap is empty" : "Hashmap is not empty");
        // returns: Hashmap is not empty

        // get the value of a specified key
        String val = usernames.get("Alyssa");
        System.out.println(val);
        // returns "Alyssa505"

        // Create a program that will iterate through all the elements in a hash map
        for (Map.Entry me : usernames.entrySet()){
            System.out.println("Key: " + me.getKey() + " & Value: " +me.getValue());
        }
        // returns:
        // Key: Victor & Value: Victor572
        // Key: Jose & Value: JoseV211
        // Key: Alyssa & Value: Alyssa505
        // Key: Angela & Value: Angie101
        // Key: Stephen & Value: stephen001


        // create a program to test
        System.out.println(usernames.containsKey("Alyssa") ? "Alyssa is in the list" : "Alyssa isnt on the list");

        //
        Set keySet = usernames.keySet();
        System.out.println("the keyset values are: " + keySet);

        // collection view of the VALUES
        System.out.println("Collection view of values: " +usernames.values());


    }
}
