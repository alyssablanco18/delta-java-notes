import java.util.Scanner;

public class ConsoleIO {
    public static void main(String[] args) {
        // PRINTING OUT DATA IN THE CONSOLE

        String cohort = "Delta";
//        System.out.println(cohort);

        String greeting = "Bonjour";
//        System.out.println(greeting);

        // println vs print
//        System.out.print(cohort);
//        System.out.print(greeting);

        System.out.println();
        // souf / printf / format
//        System.out.printf("%s, %s!%n", greeting, cohort);
//        System.out.printf("%s, %s!", greeting, cohort);
        // FORMAT SPECIFIERS


        double price1 = 23.451;
        double price2 = 50;
        double price3 = 5.40;
        double total = (price1 + price2 + price3);
//        System.out.println(total);
//        System.out.printf("Your total : $%7.2f%n",total);
//        System.out.printf("Your total : $%7.3f%n",total);
//        System.out.printf("Your total : $%7f%n",total);

        // money amount, getting two decimal places
//        System.out.printf("Your total : $%.2f%n",total);


        //=====================================================================================
        // SCANNER CLASS - get input from the console.
        Scanner scanner = new Scanner(System.in);
        // create a prompt
        System.out.println("Enter your name: "); // prompt the user to enter data

        // create a variable that will obtain whatever the user enters
        String userInput = scanner.nextLine(); // obtaining the value of the user input

        // display the data that the user entered
        System.out.println("thank you, you entered: \n" + userInput);


        // SCANNER CLASS TO RETRIEVE A NUMBER
        System.out.println(" enter an integer: ");

        // getting an integer...
        int userInt = scanner.nextInt();
        System.out.println("The integer you entered was " + userInt);

        // for getting decimal numbers...
//        double userInt = scanner.nextDouble();
//        System.out.printf("The integer you entered was %.2f", userInt);
    }
}
