package BookList;

import java.util.ArrayList;
import java.util.Scanner;

public class BookListFeatures {

    ArrayList<Book> bookList = new ArrayList<>();

    // method to add a new book to the booklist
    public void addingBook() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a title: ");
        String titleInput = sc.nextLine();

        System.out.println("Enter the author: ");
        String authorInput = sc.nextLine();

        Book newBook = new Book(authorInput,titleInput);

        bookList.add(newBook);

        System.out.println("Book has been added successfully!");
//        System.out.println(bookList.get(newBook));

        displayList();
    }

    // method to display the booklist
    public void displayList() {
        for (Book book : bookList) {
            System.out.println(book.getAuthor() + " " + book.getTitle());
        }
    }


}
