package ReportCard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class GradingApp {
    public static void main(String[] args) {
//        HashMap<String> students = new HashMap<>();
        do {
            System.out.println();
            System.out.println("WELCOME!!!");
            System.out.println("Here are the names of our students: ");
            System.out.println("| Sally  | | Timmy | | Jonah | | Sarah |");
            System.out.println();
            System.out.println("What students info would you like to access?");
            System.out.println("Enter Name: ");


            Scanner sc = new Scanner(System.in);
            String userInput = sc.nextLine();

//            System.out.println(students);

            if (userInput.equals("Sally")) {
                System.out.println(Student.sallysInfo());
            } else if (userInput.equals("Timmy")) {
                System.out.println(Student.timmysInfo());
            } else if (userInput.equals("Jonah")) {
                System.out.println(Student.jonahsInfo());
            } else if (userInput.equals("Sarah")) {
                System.out.println(Student.sarahsInfo());
            } else if (userInput.equals("exit")) {
                break;
//                System.out.println("Please enter a name that's on the list!");
            }

        } while (true);
    }
}
