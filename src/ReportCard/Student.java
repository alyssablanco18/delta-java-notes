package ReportCard;

public class Student<averageGrade, counter> {
    public Student() {

    }

    // PRIVATE PROPERTIES
    private String name;
    private double grade;

    // CONSTRUCTOR
    public Student(String name, double grade) {
        this.name = name;
        this.grade = grade;
    }


    // METHODS
    // method to return students name
    public String getName() {
        return this.name;
    }

    // method to add givin grade to grades property
//    public void addGrade() {
//        return
//    }

    // method that returns the average of the students grade


    // Sallys info method
    public static String sallysInfo() {
        return ("Name: Sally \nGitlab UserName: Sallbear\ncurrentAverage = 82.4");
    }

    public static String timmysInfo() {
        return ("Name: Timmy \nGitlab UserName: Timmy023\ncurrentAverage = 75.3");
    }

    public static String jonahsInfo() {
        return ("Name: Jonah \nGitlab UserName: Jonahhhhh1\ncurrentAverage = 91.4");
    }

    public static String sarahsInfo() {
        return ("Name: Sarah \nGitlab UserName: Sara5200\ncurrentAverage = 88.2");
    }


}
