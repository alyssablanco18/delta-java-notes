public class CarMain {
    public static void main(String[] args) {
        Car c1 = new Car("Chevy","Malibu");
        Car c2 = new Car("Ford", "Fiesta");

        Car c3 = new Car("Honda","Civic",2021);

        System.out.println(c1.getCarInfo()); // Chevy Malibu
        System.out.println(c2.getCarInfo()); // Ford Fiesta
        System.out.println(c3.getCarInfo()); // Honda Civic

        System.out.println(c3.getYear()); // 2021
    }
}
